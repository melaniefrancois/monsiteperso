// Import des icônes Fontawesome dont ont a besoin 

import { library, dom } from "@fortawesome/fontawesome-svg-core";


// Mes services
import { faCode } from "@fortawesome/free-solid-svg-icons/faCode";
import { faChartLine } from "@fortawesome/free-solid-svg-icons/faChartLine";
import { faSmileBeam } from "@fortawesome/free-solid-svg-icons/faSmileBeam";
import { faPalette } from "@fortawesome/free-solid-svg-icons/faPalette";
import { faPaintBrush } from "@fortawesome/free-solid-svg-icons/faPaintBrush";
import { faUserCheck } from "@fortawesome/free-solid-svg-icons/faUserCheck";



import { faNewspaper } from "@fortawesome/free-solid-svg-icons/faNewspaper";
import { faCogs } from "@fortawesome/free-solid-svg-icons/faCogs";
import { faExclamationTriangle } from "@fortawesome/free-solid-svg-icons/faExclamationTriangle";
import { faInfoCircle } from "@fortawesome/free-solid-svg-icons/faInfoCircle";
import { faQuestionCircle } from "@fortawesome/free-solid-svg-icons/faQuestionCircle";

import { faGithub } from "@fortawesome/free-brands-svg-icons/faGithub";
import { faFacebook } from "@fortawesome/free-brands-svg-icons/faFacebook";
import { faTwitter } from "@fortawesome/free-brands-svg-icons/faTwitter";

import { faLinkedinIn } from "@fortawesome/free-brands-svg-icons/faLinkedinIn";

import { faSteam } from "@fortawesome/free-brands-svg-icons/faSteam";
import { faPatreon } from "@fortawesome/free-brands-svg-icons/faPatreon";





library.add(faCode, faChartLine, faPalette, faPaintBrush, faSmileBeam, faUserCheck, faLinkedinIn, faGithub, faFacebook, faTwitter, faCogs, faSteam, faExclamationTriangle, faInfoCircle, faQuestionCircle, faPatreon, faNewspaper);
dom.watch();