const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");

module.exports = {
	entry: {
		lmio: './src/main.js',
	},
	output: {
		path: path.join(__dirname, './assets'),
		filename: 'js/[name].js',
		publicPath: '/'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use : [
					{
						loader: 'babel-loader',
						options: 
						{
							presets: [
								["env", 
								{
					      			"targets": 
					      			{
						        		"browsers": ["last 2 versions", "ie >= 9"]
						      		}
						    	}]
						    ],
							plugins: ['syntax-dynamic-import']
						}
					}
				]
			}
		]
	},
	plugins: [
		new CleanWebpackPlugin(["assets/js"])
	]
};